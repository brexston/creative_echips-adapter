$(document).ready(function($){
	var opt = {
		success: showRespectForm
		}	
	$('form.ajax').ajaxForm(opt);
});

function showRespectForm(resp, status, xhr, $form){

	$form.addClass('send').find('.input-required').removeClass('input-valid');
	$('input[type=text]').val('');
	$('input[type=tel]').val('');
	$('textarea').val('');
	$('input[type=submit]').attr('disabled', false);
	location="/thanks.html";

}

function showRequest(formData, jqForm, options) { 
	$('input[type=submit]').attr('disabled', true);
	return true; 
}