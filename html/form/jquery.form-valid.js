function isValidEmail (email, strict) {
	if ( !strict ) email = email.replace(/^\s+|\s+$/g, '');
	return (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);
};

$( document ).ready(function() {

	$('form').submit(function(){
		$(this).find('input').removeClass('input-error');
		$(this).find('input').parent().find('.form_valid_alert').remove();
		valid = true;
		$(this).find('input.input-required').each(function(){
			if ( $(this).hasClass('input-mail') ) {
				if ( !isValidEmail($(this).val()) ) {
					valid = false;
					$(this).addClass('input-error');
					$(this).parent().append("<div class='form_valid_alert'>Введите корректный Email</div>");
					$(this).click(function(){ $(this).parent().find('.form_valid_alert').remove(); });
				};
			};
			if ( $(this).val()=='') {
				valid = false;
				$(this).parent().find('.form_valid_alert').remove();
				$(this).addClass('input-error');
				if ( $(this).hasClass('input_select') ) {
					$(this).addClass('input-error');
					$(this).parent().append("<div class='form_valid_alert'>Выберите значение</div>");
					$(this).parent().children('.slct').click(function(){ $(this).parent().find('.form_valid_alert').remove(); });
				} else {
					$(this).parent().append("<div class='form_valid_alert'>Заполните это поле</div>");
					$(this).click(function(){ $(this).parent().find('.form_valid_alert').remove(); });
				};
			};
		});
		return valid;
	});

});