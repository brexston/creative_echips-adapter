$(document).ready(function(){
	$('ul.shop-tabs__caption').on('click', 'li', function() {
		$(this)
			.addClass('active').siblings().removeClass('active')
			.closest('div.shop-tabs').find('div.shop-tabs__content').removeClass('active').eq($(this).index()).addClass('active');
			$('body, html').animate({scrollTop: $('.shop-tabs').offset().top}, 1500);
			return false;
	});

	var popupName1 = new ConstractPopup('#orderPopup'); 

	$('.button--buy, .button--order').on('click',  function(e) {
		
		e.preventDefault();



		var text = $(this).attr("data-text");
		var name = $(this).attr("data-name"); 


		$('#orderPopup').find('input[name="widget_3"]').val(name);
		$('#form-text').text(text); 

		$(popupName1.ppBg).dialog('open'); /* открытие попапа с именем popupName */
	});

	$(".input_phone").mask("8 (999) 999-99-99");

});
	
$(window).scroll(function() {
	$('.shop-tabs').each(function() {
		
		var elemPos = $(this).offset().top;
		var topOfWindow = $(window).scrollTop();
		
		var bottom =  elemPos - $(this).height() - 120;

		if ( bottom < topOfWindow )  {
			$('#mMenu').addClass('fixed');
			$('#mMenuPlaceholer').show();
		}  
		else {
			$('#mMenu').removeClass('fixed');
			$('#mMenuPlaceholer').hide();
		}
	});
});

