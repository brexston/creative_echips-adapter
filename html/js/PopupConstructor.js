/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



    function ConstractPopup(ppBg, ppElements, ppCloseBtns) {/*функция-конструктор класса для конструирования попапа*/
        this.ppBg = ppBg;
        var ppElements = ppElements;
        var ppCloseBtns = ppCloseBtns;

        var ppWidth = $(ppBg).width();
        var ppHeight = $(ppBg).height();

        //ui-dialog

        $(this.ppBg).dialog({/* инициализируем диалог */
            autoOpen: false, /* но не открываем сразу */
            width: ppWidth, /* ширина окна */
            height: ppHeight, /* высота окна */
            modal: true,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            resizable: false, /* статичный размер окна */
            open: function(event, ui) {

                $(event.target).dialog('widget')
                    .css({ position: 'fixed' })
                    .css({ 'margin-top': -ppHeight / 2 })
                    .css({ top: '50%' });


                $('div.ui-widget-overlay').on('click', function() {
                    $(ppBg).dialog('close');
                });
            }
        });

        $(ppBg).parent()
            .css({ position: 'fixed' })
            .css({ 'margin-top': -ppHeight / 2 })
            .css({ top: '50%' });

        $(ppCloseBtns).on('click', 'a', function(e) {
            e.preventDefault();
            $(ppBg).dialog('close');
        });
    }
